<?php
//loop and query the product dynamic list for the index.php page from XML file
function dynamic_list_pizza() {
	$xml = simplexml_load_file ("menu.xml");
	$items = $xml->xpath('category[1]/item');
	foreach($items as $item) {
			$id = $item->pid;
			$pname = $item->name;
			$large = $item->large;
			$small = $item->small;
			$dynamicList= '<div class="row">
						  <div class="col-md-4"><img src="img/'. $item->name . '.jpg" width="160px" height="100px"></div>
						  <div class="col-md-4"><center>' . $item->name . '</br>Price: Large $' . $item->large . ' Small $' . $item->small . '</center></div>
						  <div class="col-md-4">
							  <!-- Add to Cart form -->
							  <form id="cart" name="cart" method="POST" action="cart.php">
								  <input type="hidden" name="pid" id="pid" value=' . $id . '/>
								  <input type="hidden" name="pname" id="pname" value=' . $pname . '/>
								  <select name="price"><option value="'. $large .'">Large</option><option value="'. $small .'">Small</option></select>
								  <input type="number" name="quantity" min="1" max="10">
								  </br><input type="submit" value="Add to Cart"/></center>
							  </form>
						  </div><!-- end col-md-4 -->
						</div><!-- end row --></br>';
			
		echo $dynamicList; 
	} 
}

function special_dinners() {
	$xml = simplexml_load_file ("menu.xml");
	$items = $xml->xpath('category[2]/item');
	foreach($items as $item) {
			$id = $item->pid;
			$pname = $item->name;
			$large = $item->large;
			$small = $item->small;
			$dynamicList= '<div class="row">
						  <div class="col-md-4"><img src="img/'. $item->name . '.jpg" width="160px" height="100px"></div>
						  <div class="col-md-4"><center>' . $item->name . '</br>Price: Large $' . $item->large . '</center></div>
						  <div class="col-md-4">
							  <!-- Add to Cart form -->
							  <form id="cart" name="cart" method="POST" action="cart.php">
								  <input type="hidden" name="pid" id="pid" value=' . $id . '/>
									<input type="hidden" name="pid" id="pid" value=' . $large . '/>
								  <input type="hidden" name="pname" id="pname" value=' . $pname . '/>
								  <input type="number" name="quantity" min="1" max="10">
								  </br><input type="submit" value="Add to Cart"/></center>
							  </form>
						  </div><!-- end col-md-4 -->
						</div><!-- end row --></br>';
			
		echo $dynamicList; 
	} 
}

function salads() {
	$xml = simplexml_load_file ("menu.xml");
	$items = $xml->xpath('category[3]/item');
	foreach($items as $item) {
			$id = $item->pid;
			$pname = $item->name;
			$large = $item->large;
			$small = $item->small;
			$dynamicList= '<div class="row">
						  <div class="col-md-4"><img src="img/'. $item->name . '.jpg" width="160px" height="100px"></div>
						  <div class="col-md-4"><center>' . $item->name . '</br>Price: Large $' . $item->large . ' Small $' . $item->small . '</center></div>
						  <div class="col-md-4">
							  <!-- Add to Cart form -->
							  <form id="cart" name="cart" method="POST" action="cart.php">
								  <input type="hidden" name="pid" id="pid" value=' . $id . '/>
								  <input type="hidden" name="pname" id="pname" value=' . $pname . '/>
								  <select name="price"><option value="'. $large .'">Large</option><option value="'. $small .'">Small</option></select>
								  <input type="number" name="quantity" min="1" max="10">
								  </br><input type="submit" value="Add to Cart"/></center>
							  </form>
						  </div><!-- end col-md-4 -->
						</div><!-- end row --></br>';
			
		echo $dynamicList; 
	} 
}

function populating_cart() {
	//check if any product was submitted to the cart
	if(isset($_POST['pid']) && $_POST['pid']!=='') {
		$pid = $_POST['pid'];
		$quantity = $_POST['quantity'];
		$pname = $_POST['pname'];
		$price = $_POST['price'];
		$i = 0;
		$wasFound = false;
		
		//add the item to the cart if it is empty
		if(!isset($_SESSION["cart_array"])) {
		$_SESSION["cart_array"] = array(0 => array("item_id" => $pid,"pname" => $pname,"quantity" => $quantity,"price" => $price));
		} else {
		//Run if the cart has a least one item in it
		$wasFound = true;
			if($wasFound == true) {
			array_push($_SESSION["cart_array"], array("item_id" => $pid,"pname" => $pname,"quantity" => $quantity,"price" => $price));
			}
		}
	} else {
		echo "Your cart is empty";
	}
}

function empty_cart() {
	//If empty you cart link is pressed
	if(isset($_GET['cmd']) && $_GET['cmd'] == "emptycart") {
		unset($_SESSION["cart_array"]);
	}
}
	
function render_cart() {
	//Display cart content in cart.php		
	$cartOutput="";
	$i = 0;
	$cartTotal = "";
	
		foreach ($_SESSION["cart_array"] as $each_item) { 
			$cartOutput.= "<tr>";
			$cartOutput.= "<td><center>" . substr($each_item['pname'], 0, -1) . "</center></td>";
			$cartOutput.= "<td><center><img src='img/" . substr($each_item['pname'], 0, -1) . ".jpg' width='160px' height='120px'></center></td>";
			$cartOutput.= "<td><center>$" . $each_item['price'] . "</center></td>";
			$cartOutput.= "<td><center>" . $each_item['quantity'] . "</center></td>";
			$cartOutput.= "<td><center>$" . $each_item['price'] * $each_item['quantity'] . "</center></td>";
			$cartOutput.= '<td><center>' . $i . '<form action="cart.php" method="post"><input name="deleteBtn' . $each_item['pname'] . '" type="submit" value="Remove" /><input name="item_to_remove" type="hidden" value="'. $i .'" /></form></center></td>';
			$cartOutput.= "</tr>";
			$i++;
			$cartTotal = ($each_item['price'] * $each_item['quantity']) + $cartTotal;
			
		}//close foreach loop
	echo $cartOutput;
	echo "<center><b>Total in cart:</b> $" . $cartTotal . "</center>";	
}	
	
function remove_items() {
	//Remove items
	if (isset($_POST['item_to_remove'])) {
		// Access the array and run code to remove that array index
		$item_to_remove = $_POST['item_to_remove'];
		echo "<pre>";
		print_r ($_SESSION["cart_array"]);
		echo "</pre>";
		unset($_SESSION["cart_array"]["$item_to_remove"]);
		sort($_SESSION["cart_array"]);
		echo "<pre>";
		print_r ($_SESSION["cart_array"]);
		echo "</pre>";
		header('Location: cart.php');
		}
}	 

?>
