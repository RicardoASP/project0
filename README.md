# README #

Project for CS E-75 college course. Also, first time using bitbucket and public repositories. Questions or comments please refer to the wiki (I think that is the best place for that). 
Languages: HTML, CSS, XML and PHP

### What is this repository for? ###

* Project was to create a basic market place type website for a pizzeria, which wanted to sell their products online. Use XML file as database (no mysql for now). Scope of the project is to have a product display page and functional cart for checkout (no payment gateway).
* Version 1.0

### How do I get set up? ###

* I am using Ubuntu as for my DevBox
* No mysql
* Not sure what else to inform contributors on how to get set up, please advise. 

### Contribution guidelines ###

* Not sure what kind of guidelines to give for contributors, please advise.

### Who do I talk to? ###
Ricardo Sanchez
Use wiki for communications