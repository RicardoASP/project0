<?php include ('header.php'); ?>
	
      <div class="blog-header">
        <h3 class="blog-title">Order for delivery here:</h3>
        <p class="lead blog-description">You will have your food ready and in front of your door in less than 30min.</p>
      </div>

      <div class="row">

        <div class="col-sm-8 blog-main">
					
					<center><h2>Pizzas</h2></center>
					<?php dynamic_list_pizza(); ?>
					</br>
					<center><h2>Special Dinners</h2></center>
					<?php special_dinners(); ?>
					</br>
					<center><h2>Salads</h2></center>
					<?php salads(); ?>

        </div><!-- /.blog-main -->
		  
<?php include ('sidebar.php'); ?>

<?php include ('footer.php'); ?>
 